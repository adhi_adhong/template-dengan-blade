<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function master(){
        return view('layout.master');
    }
    public function dashboard(){
        return view('dashboard.dashboard');
    }
    public function table(){
        return view('table.table');
    }
    public function datatable(){
        return view('table.datatable');
    }

}

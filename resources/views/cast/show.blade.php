@extends('layout.master')

@section('judul')
    Detail Cast
@endsection

@section('content')
<p>Show Cast {{$cast->id}}</p>
<h2>Nama : {{$cast->nama}}</h2>
<p>Umur : {{$cast->umur}}</p>
<p>Biodata : {{$cast->bio}}</p>
<a href="/cast" class="btn btn-warning mb-3" >Kembali</a>
@endsection
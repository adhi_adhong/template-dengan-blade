@extends('layout.master')

@section('judul')
    Edit Data Cast {{$cast->nama}}
@endsection

@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label >Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$cast->nama}}" id="nama" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label >Umur</label>
            <input type="number" class="form-control" name="umur" value="{{$cast->umur}}" id="umur" placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Biodata</label>
            <textarea name="bio" class="form-control" cols="100" rows="5" placeholder="Masukkan Biodata">{{$cast->bio}}</textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
@endsection
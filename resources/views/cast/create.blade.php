@extends('layout.master')

@section('judul')
    Tambah Data Cast 
@endsection

@section('content')

<div>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama"  placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number3" class="form-control" name="umur"  placeholder="Masukkan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">Biodata</label><br>
                <textarea class="form-control" name="bio"  cols="100" rows="5" placeholder="Masukkan Biodata"></textarea>
                {{-- <input type="text" class="form-control" name="bio"  placeholder="Masukkan Biodata"> --}}
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection
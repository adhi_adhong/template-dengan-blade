<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buar Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
    <label >First name :</label><br>
    <input type="text" name="namadepan"><br><br>
    <label >Last name :</label><br>
    <input type="text" name="namabelakang"><br><br>
    <label >Gender :</label><br>
    <input type="radio" name="gender" value="Male">Male<br>
    <input type="radio" name="gender" value="Female">Female<br>
    <input type="radio" name="gender" value="Other">Other<br><br>
    <label >Nationality :</label><br>
    <select name="warganegara" id="warganegara">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Thailand">Thailand</option>
        <option value="Jepang">Jepang</option>
    </select><br><br>
    <label >Language Spoken :</label><br>
    <input type="checkbox" name="bahasa" value="Bahasa Indonesia">Bahasa Indonesia<br>
    <input type="checkbox" name="bahasa" value="English">English<br>
    <input type="checkbox" name="bahasa" value="Other">Other<br><br>
    <label >Bio :</label><br>
    <textarea name="bio" cols="30" rows="10"></textarea><br>
    <br>
    <input type="submit" value="Sign Up">
    </form>
    
</body>
</html>